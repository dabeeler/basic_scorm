# Basic SCORM Package - A Template

## What is this?
While investigating how to transform an HTML + JavaScript game into a package compatible with Learning Management Systems (LMS), I wanted a basic template to play with as I learned about SCORM and what I needed to transform my existing code.

Several examples I found were no longer available (old links) or did not work with the LMS used at the University of Dayton (Isidore, running on the Sakai platform). Although there are extensive [sample packages on SCORM.com](https://scorm.com/scorm-explained/technical-scorm/golf-examples/), they would not display on our LMS. Now that I know more about how everything works, perhaps I could troubleshoot that issue.

Regardless, I found pipwerks' SCORM Wrapper for JavaScript and then found a write-up of adding SCORM to an HTML file, and things were finally falling into place. The links for these sources are below. I did not see a complete repository based on the tutorial, so I decided to create this repo. This repo does not add anything of substance; it is a starting template. Check the wrapper for updates, this repo uses Release v1.1.20180906.

**Important Links:**
- Philip Hutchison (pipwerks) - ["Adding SCORM code to an HTML file using the pipwerks SCORM wrapper"](https://pipwerks.com/2008/05/08/adding-scorm-code-to-an-html-file/)
- [SCORM Wrapper for JavaScript (GitHub)](https://github.com/pipwerks/scorm-api-wrapper) (SCORM 1.2 and SCORM 2004; MIT-style license)
- [SCORM Manifests (GitHub)](https://github.com/pipwerks/SCORM-Manifests)
- [SCORM API Wrapper Information (pipwerks)](https://pipwerks.com/laboratory/scorm/api-wrapper-javascript/) -- all-encompassing reference for the amazing work pipwerks has done.

## What changes were made from the linked tutorial?

- `pipwerks.scorm` was changed to `pipwerks.SCORM`
- As discussed in the "Prefer SCORM 2004 over SCORM 1.2, the `cmi` calls were changed:
    - `cmi.core.completion_status` was changed to `cmi.completion_status` ()
    - `cmi.core.learner_name` was changed to `cmi.learner_name`

For `cmi` values, the [SCORM Run-Time Reference Guide](https://scorm.com/scorm-explained/technical-scorm/run-time/run-time-reference/) is great.

## How do I use this?

If you are testing on a similar system that expects **SCORM 2004 3rd Edition** packages, you can download the `basic_scorm.zip` file, upload it to your SCORM Player, and give it a test. This package displays a simple form that pulls in the learner's name and has them certify that they have read the materials provided to them and marks the "lesson" as complete.

If you want to customize:

- *(Optional)* Make changes to `dist/index.html`.
- *(Optional)* Make changes to `dist/imsmanifest.xml`.
    - Update line 2 - specify a course ID.
    - Update lines 21 and 23 - specify a more meaningful title.
    - Update lines 80+ - specify any additional files referenced
- Zip the files and folders within `dist` - don't include the parent directory in the final zip file. If you are on Windows and have 7-Zip installed, you might be able to use the `package.json` and run `npm run bundle`. But you can always customize this file for your system if you will be playing and testing.

## License

The wrapper created by pipwerks is licensed for MIT. I set no conditions for my bundling here and will use the Unlicense for what I've done. Please abide by the licensing set out by pipwerks as you make changes and customize this template.
